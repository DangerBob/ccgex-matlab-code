function val = Massflow (varargin)
p1 = varargin{1};
p2 = varargin{2};
T1 = varargin{3};
Di = varargin{4};
Do = varargin{5};
mu1 = varargin{6};
rho1 = p1/(287*T1); 
dp = p1-p2;
eps1 = 1-(0.41+0.35*(Di/Do)^4)*dp/(1.4*p1);
if p2/p1<0.75, eps1=1; end
Re0 = 1e6;
Re_D = fzero(@(x)DeltaMF(x,dp,rho1,mu1,eps1,Di,Do),Re0);
val = Re_D*pi/4*Do*mu1;
if isnan(val), val=[]; end
end

function  dMF = DeltaMF (varargin)
ip = inputParser;
ip.addRequired('Re_D', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('dp', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('rho1', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('mu1', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('eps1', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('Di', @(x)isscalar(x) & isnumeric(x));
ip.addRequired('Do', @(x)isscalar(x) & isnumeric(x));
ip.parse(varargin{:});
beta = ip.Results.Di/ip.Results.Do;
qm_Re_D = ip.Results.Re_D*pi/4*ip.Results.Do*ip.Results.mu1;
C = ReaderHarrisGallagher(ip.Results.Re_D, ip.Results.Do, beta);
qm_or = C*ip.Results.eps1*pi/4*ip.Results.Di^2*sqrt(2*ip.Results.dp*ip.Results.rho1/(1-beta^4));
dMF = qm_or-qm_Re_D;
end

function [ C ] = ReaderHarrisGallagher (Re_D, Do, beta)
%READERHARRISGALLAGHER Reader-Harris/Gallagher equation
    A_c = (19000*beta/Re_D)^0.8;
    C = 0.5961 + 0.0261*beta^2 - 0.216*beta^8 + ...
        0.000521*(beta*1e6/Re_D)^0.7 + ...
        (0.0188+0.0063*A_c)*beta^3.5 * (1e6/Re_D)^0.3;
    if Do < 0.0712
        C = C + 0.011*(0.75-beta)*(2.8-Do*1e3/25.4);
    end
end
